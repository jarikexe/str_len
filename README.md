<h1>USAGE</h1>

1. write a sting as a first parameter of function
1. go to a browser
3. run the script str_len.php or str_len2.php

<h1>exemple</h1>
```php
<?php
  $stroke = "Some test stroke";
  echo str_len($stroke); //return 16

```

<h1> Two different solutions </h1>

The main difference is in function which count symbols in a stroke.

1st one is done by an array of all symbols in the stroke.

```php
<?php
function count_symbols($s){
  $str2 = str_split($s);
  $i = 0;
  // можно еще было вернуть количество элемента масива, но это совсем читерство.
  if(!empty($s)){
    foreach ($str2 as $k=> $v) {
      $i++;
    }
    return $i;
  }else return 0;
}
```

2nd one is done by do while loop.

```php
<?php
function count_symbols($s){
  $i = 1;
    if(!empty($s)){
    do {
      $i++;
    } while ($s[$i] || $s[$i] === "0");
    return  $i;
  }else return 0;
}
```
