<?php
//самый очевидный вариант
function count_symbols($s){
  $str2 = str_split($s);
  $i = 0;
  // можно еще было вернуть количество элемента масива, но это совсем читерство.
  if(!empty($s)){
    foreach ($str2 as $k=> $v) {
      $i++;
    }
    return $i;
  }else return 0;
}

// нимного мение очевидная для меня функция

function errors($type, $line){
  echo "<b>Warning:</b> str_len() expects parameter 1 to be string, ".$type." given in <b>". __FILE__ . "</b> on line <b>" . $line ."</b>";
}

function  str_len($s){
  //тут есть смысл воспользоватся свитчом но можна и так.
  if(gettype($s) === "array") errors(gettype($s),  __LINE__);
  if(gettype($s) === "NULL") return 0;
  if(gettype($s) === "boolean" AND $s === true) return 1;
  if(gettype($s) === "boolean" AND $s === false) return 0;
  if(gettype($s) === "resource") errors(gettype($s),  __LINE__);
  if(gettype($s) === "string" || gettype($s) === "integer" || gettype($s) === "double") return count_symbols($s);
  if(gettype($s) === "object") errors(gettype($s), __LINE__);
}

// просто пустой класс для пиме
class ClassName
{
  function __construct($arg)
  {

  }
}

$testVar = "Lorem";
//$testVar = fopen("foo", "w");
//$testVar = ['afjkd', 'klfjaklj', 'jakdfjklj'];
//$testVar = 108000000;
//$testVar = 3.14;
//$testVar = true;
// $testVar = false;
// $testVar = new ClassName(1);
//$testVar = "";



echo "Nominal ";
echo strlen($testVar)."<br>";
echo "My result " . str_len($testVar);
